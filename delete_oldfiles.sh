#! /bin/bash
currdate=$(gdate -d "$(gdate "+%Y-%m-%d")" +'%s')
olddate=$(date -j -v-30d -f "%s" "$currdate" "+%s")
count=0

for i in $( gls -ld --time-style long-iso /Users/jpalharini/atlassian/sac/* | awk '{print $6}' | sort );
	do
		datemillis=$(gdate -d "$i" +'%s')
		while [[ $datemillis -le $olddate ]]
			do
				let count=$count+1
			done
		if [ $count -ne 0 ];
			then
				list=$( gls -ld --time-style long-iso /Users/jpalharini/atlassian/sac/* | awk '{print $6,"\011",$8}' | sort -k1 | head -n $count )
				for x in $( cat "$list" | awk '{print $2}');
					do
						echo "moving..."
						mv -r $x /Users/jpalharini/atlassian/sac/.limbo
					done
		else
			echo "No files older than 30 days"
			break
		fi
	done